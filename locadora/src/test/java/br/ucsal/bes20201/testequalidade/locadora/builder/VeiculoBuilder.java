package br.ucsal.bes20201.testequalidade.locadora.builder;

import br.ucsal.bes20201.testequalidade.locadora.dominio.Modelo;
import br.ucsal.bes20201.testequalidade.locadora.dominio.Veiculo;
import br.ucsal.bes20201.testequalidade.locadora.dominio.enums.SituacaoVeiculoEnum;

public class VeiculoBuilder {

	private Veiculo veiculo;

	public static VeiculoBuilder veiculo() {
		return new VeiculoBuilder();
	}

	public VeiculoBuilder cujaPlacaEh(String placa) {
		veiculo.setPlaca(placa);
		return this;
	}

	public VeiculoBuilder cujoAnoEh(Integer ano) {
		veiculo.setAno(ano);
		return this;
	}

	public VeiculoBuilder cujoModeloEh(Modelo modelo) {
		veiculo.setModelo(modelo);
		return this;
	}

	public VeiculoBuilder cujoValorDiariaEh(Double valorDiaria) {
		veiculo.setValorDiaria(valorDiaria);
		return this;
	}

	public VeiculoBuilder cujaSituacaoEh(SituacaoVeiculoEnum situacao) {
		veiculo.setSituacao(situacao);
		return this;
	}

	public Veiculo build() {
		return veiculo;
	}

}