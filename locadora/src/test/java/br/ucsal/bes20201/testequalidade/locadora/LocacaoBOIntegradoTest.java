package br.ucsal.bes20201.testequalidade.locadora;

import static org.junit.jupiter.api.Assertions.assertEquals;
import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import br.ucsal.bes20201.testequalidade.locadora.builder.VeiculoBuilder;
import br.ucsal.bes20201.testequalidade.locadora.business.LocacaoBO;
import br.ucsal.bes20201.testequalidade.locadora.dominio.Modelo;
import br.ucsal.bes20201.testequalidade.locadora.dominio.Veiculo;
import br.ucsal.bes20201.testequalidade.locadora.dominio.enums.SituacaoVeiculoEnum;
import br.ucsal.bes20201.testequalidade.locadora.persistence.VeiculoDAO;

public class LocacaoBOIntegradoTest {

	private static List<Veiculo> veiculos = new ArrayList<Veiculo>();

	private void escolherVeiculos() {
		veiculos.add(VeiculoBuilder.veiculo().cujaPlacaEh("p1").cujoAnoEh(2016).cujoModeloEh(new Modelo("m1"))
				.cujoValorDiariaEh(100.0).cujaSituacaoEh(SituacaoVeiculoEnum.DISPONIVEL).build());
		veiculos.add(VeiculoBuilder.veiculo().cujaPlacaEh("p2").cujoAnoEh(2017).cujoModeloEh(new Modelo("m2"))
				.cujoValorDiariaEh(150.0).cujaSituacaoEh(SituacaoVeiculoEnum.DISPONIVEL).build());
		veiculos.add(VeiculoBuilder.veiculo().cujaPlacaEh("p3").cujoAnoEh(2018).cujoModeloEh(new Modelo("m3"))
				.cujoValorDiariaEh(200.0).cujaSituacaoEh(SituacaoVeiculoEnum.DISPONIVEL).build());
		veiculos.add(VeiculoBuilder.veiculo().cujaPlacaEh("p4").cujoAnoEh(2019).cujoModeloEh(new Modelo("m4"))
				.cujoValorDiariaEh(250.0).cujaSituacaoEh(SituacaoVeiculoEnum.DISPONIVEL).build());
		veiculos.add(VeiculoBuilder.veiculo().cujaPlacaEh("p5").cujoAnoEh(2020).cujoModeloEh(new Modelo("m5"))
				.cujoValorDiariaEh(300.0).cujaSituacaoEh(SituacaoVeiculoEnum.DISPONIVEL).build());
	}

	private void pedirVeiculos() {
		for (int i = 0; i < veiculos.size(); i++) {
			VeiculoDAO.insert(veiculos.get(i));
		}
	}

	@Test
	public void testarCalculoValorTotalLocacao5Veiculos3Dias() {
		escolherVeiculos();
		pedirVeiculos();
		assertEquals(1000.0, LocacaoBO.calcularValorTotalLocacao(veiculos, 3));
	}

}